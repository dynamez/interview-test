//
//  SourceViewListModel.swift
//  CencosudTest
//
//  Created by Felipe Valdes on 2/16/19.
//  Copyright © 2019 Felipe Valdes. All rights reserved.
//


import Foundation

class SourceListViewModel : NSObject {
    
    @objc dynamic private(set) var sourceViewModels :[SourceViewModel] = [SourceViewModel]()
    private var token :NSKeyValueObservation?
    var bindToSourceViewModels :(() -> ()) = {  }
    private var webservice :Webservice
    
    init(webservice :Webservice) {
        
        self.webservice = webservice
        super.init()
        
        token = self.observe(\.sourceViewModels) { _,_ in
            self.bindToSourceViewModels()
        }
        
        
        populateSources()
    }
    
    func invalidateObservers() {
        self.token?.invalidate()
    }
    
    func populateSources() {
        
        self.webservice.loadSources { [unowned self] sources in
            self.sourceViewModels = sources.compactMap(SourceViewModel.init)
            //print(self.sourceViewModels)
        }
    }
    
    func addSource(source: SourceViewModel) {
        self.sourceViewModels.append(source)
    }
    
    func source(at index:Int) -> SourceViewModel {
        return self.sourceViewModels[index]
    }
}

class SourceViewModel : NSObject {
    
    var id :NSNumber?
    var name :String
    var body :String
    var rate: NSNumber?
    var poster_url :String?
    
    init(name :String, description: String) {
        self.name = name
        self.body = description
    }
    
    init(source :Source) {
        
        self.id = source.id
        self.name = source.name
        self.body = source.description
        self.rate = source.rate
        self.poster_url = source.poster_url
    }
}



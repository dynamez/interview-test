//
//  SourceDetailsViewController.swift
//  CencosudTest
//
//  Created by Felipe Valdes on 2/16/19.
//  Copyright © 2019 Felipe Valdes. All rights reserved.
//

import Foundation
import UIKit

class SourceDetailsTableViewController : UIViewController {
    
    var sourceViewModel :SourceViewModel!
    var urlImage :String = "http://image.tmdb.org/t/p/w500"
    
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    private func updateUI() {
        
        self.title = self.sourceViewModel.name
        self.movieTitle.text = self.sourceViewModel.name
        self.movieDescription.text = self.sourceViewModel.body
        urlImage += self.sourceViewModel.poster_url!
        movieImage.load(url: URL(string: urlImage)!)

    }
}





extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

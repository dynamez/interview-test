//
//  Constants.swift
//  CencosudTest
//
//  Created by Felipe Valdes on 2/16/19.
//  Copyright © 2019 Felipe Valdes. All rights reserved.
//

import Foundation
import UIKit

struct Maximum {
    static let allowedCharactersForDescription = 70
}

struct Cells {
    static let headline = "HeadlineTableViewCell"
    static let source = "SourceTableViewCell"
}

struct SegueIdentifier {
    
    static let showSourceDetails = "SourceDetailsTableViewController"
    
}

struct URLs {
    static let imgURL = "http://image.tmdb.org/t/p/w500"
}

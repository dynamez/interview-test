//
//  SourceTableViewCell.swift
//  CencosudTest
//
//  Created by Felipe Valdes on 2/16/19.
//  Copyright © 2019 Felipe Valdes. All rights reserved.
//


import Foundation
import UIKit

class SourceTableViewCell : UITableViewCell {
    
    @IBOutlet weak var nameLabel :UILabel!
    
    @IBOutlet weak var rate_value: UILabel!
    
    @IBOutlet weak var detailsBtn: UIButton!
    
}

//
//  Headline.swift
//  CencosudTest
//
//  Created by Felipe Valdes on 2/16/19.
//  Copyright © 2019 Felipe Valdes. All rights reserved.
//


import Foundation

class Headline {
    
    var title :String!
    var vote_average :String!
    
    init?(dictionary :JSONDictionary) {
        
        guard let title = dictionary["title"] as? String,
            let vote_average = dictionary["vote_average"] as? String else {
                return nil
        }
        
        self.title = title
        self.vote_average = vote_average
    }
}
